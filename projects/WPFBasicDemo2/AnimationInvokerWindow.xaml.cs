﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Shapes;

namespace HanoiTowerWPF
{
	/// <summary>
	/// AnimationInvokerWindow.xaml 的交互逻辑
	/// </summary>
	public partial class AnimationInvokerWindow : Window
	{
		public AnimationInvokerWindow()
		{
			InitializeComponent();


		}

		public void SetAnimation(Shape shape)
		{
			var myDoubleAnimation = new DoubleAnimation();
			myDoubleAnimation.From = 1.0;
			myDoubleAnimation.To = 0.0;
			myDoubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(5));

			//var myStoryboard = new Storyboard();
			//myStoryboard.Children.Add(myDoubleAnimation);
			//Storyboard.SetTargetName(myDoubleAnimation, shape.Name);
			//Storyboard.SetTargetProperty(myDoubleAnimation, new PropertyPath(Shape.OpacityProperty));
			//myStoryboard.Begin(this);


			shape.BeginAnimation(Shape.OpacityProperty, myDoubleAnimation);

		}

		private void btnTest1_Click(object sender, RoutedEventArgs e)
		{
			SetAnimation(rect1);
			Debug.WriteLine("Done.");

			return;

			this.Title = "Local Animation Example";
			StackPanel myStackPanel = new StackPanel();
			myStackPanel.Margin = new Thickness(20);

			// Create and set the Button.
			Button aButton = new Button();
			aButton.Content = "A Button";

			// Animate the Button's Width.
			DoubleAnimation myDoubleAnimation = new DoubleAnimation();
			myDoubleAnimation.From = 75;
			myDoubleAnimation.To = 300;
			myDoubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(5));
			myDoubleAnimation.AutoReverse = true;
			myDoubleAnimation.RepeatBehavior = RepeatBehavior.Forever;

			// Apply the animation to the button's Width property.
			aButton.BeginAnimation(Button.WidthProperty, myDoubleAnimation);

			// Create and animate a Brush to set the button's Background.
			SolidColorBrush myBrush = new SolidColorBrush();
			myBrush.Color = Colors.Blue;

			ColorAnimation myColorAnimation = new ColorAnimation();
			myColorAnimation.From = Colors.Blue;
			myColorAnimation.To = Colors.Red;
			myColorAnimation.Duration = new Duration(TimeSpan.FromMilliseconds(7000));
			myColorAnimation.AutoReverse = true;
			myColorAnimation.RepeatBehavior = RepeatBehavior.Forever;

			// Apply the animation to the brush's Color property.
			myBrush.BeginAnimation(SolidColorBrush.ColorProperty, myColorAnimation);
			aButton.Background = myBrush;

			// Add the Button to the panel.
			myStackPanel.Children.Add(aButton);
			this.Content = myStackPanel;
		}


		List<Rectangle> rects = new List<Rectangle>();
		private void btnTest2_Click(object sender, RoutedEventArgs e)
		{
			if (rects.Count > 0)
				return;

			int width = 40;
			int height = 100;
			var myStoryboard = new Storyboard();

			myStoryboard.Begin(this);
			for (int i = 0; i < 10; i++)
			{
				System.Windows.Shapes.Rectangle rect = new System.Windows.Shapes.Rectangle();
				rect.Width = width;
				rect.Height = height;
				rect.Fill = Brushes.Gold;
				rect.Stroke = Brushes.Black;
				rect.StrokeThickness = 0.5;
				rect.Name = "rect_" + i;
				canvas.Children.Add(rect);
				Canvas.SetBottom(rect, width + 100);
				Canvas.SetLeft(rect, width * 1.1 * i + 10);
				rects.Add(rect);

				var myDoubleAnimation = new DoubleAnimation();
				myDoubleAnimation.From = 1.0;
				myDoubleAnimation.To = 0.0;
				myDoubleAnimation.BeginTime = TimeSpan.FromSeconds(i*2);
				myDoubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(0.5));
				myDoubleAnimation.AutoReverse = true;

				myStoryboard.Children.Add(myDoubleAnimation);
				Storyboard.SetTarget(myDoubleAnimation, rect);
				Storyboard.SetTargetProperty(myDoubleAnimation, new PropertyPath(Shape.OpacityProperty));
			}

			myStoryboard.Begin();

			// shape.BeginAnimation(Shape.OpacityProperty, myDoubleAnimation);
		}

		// ref: https://blog.csdn.net/Senlinnnnnn/article/details/122025639
		public DoubleAnimation GetBlink(double startTime, Shape shape)
		{
			var myDoubleAnimation = new DoubleAnimation();
			myDoubleAnimation.From = 1.0;
			myDoubleAnimation.To = 0.0;
			myDoubleAnimation.BeginTime = TimeSpan.FromSeconds(startTime);
			myDoubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(0.5));
			myDoubleAnimation.AutoReverse = true;
			Storyboard.SetTarget(myDoubleAnimation, shape);
			Storyboard.SetTargetProperty(myDoubleAnimation, new PropertyPath(Shape.OpacityProperty));
			return myDoubleAnimation;
		}

		private void btnTest3_Click(object sender, RoutedEventArgs e)
		{
			if (rects.Count > 0)
			{
				var myStoryboard = new Storyboard();
				for (int i = 0; i < 10; i++)
					myStoryboard.Children.Add(GetBlink(i * 2, rects[i]));
				myStoryboard.Begin(this);
			}
		}
	}
}
