﻿/*
 * 说明：添加一些静态方法，用于对类进行操作。
 * 作者：郝伟
 * 日志：2023/04/16 新建此类。
 * 
 * 
 */ 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;

namespace wpf_sort_visualization
{
    /// <summary>
    /// 添加一些静态方法，用于对类进行操作。
    /// </summary>
    public class Helper
    {

        /// <summary>
        /// 创建TextBlock列表。
        /// </summary>
        /// <returns></returns>
        public static List<TextBlock> MakeTextBlocks()
        {
            List<TextBlock> tbs = new List<TextBlock>();
            int[] values = { 50, 125, 200, 275, 350, 425, 500, 575, 650, 725 };
            string str = "-GENERATE-";
            for (int i = 0; i < tbs.Count; i++)
            {
                // <TextBlock x:Name="Number0" Text="-" Height="50" Canvas.Left="50" Canvas.Top="150" Width="50"
                // TextAlignment="Center" Padding="10" FontSize="20" Foreground="White"  Background="Blue" />
                TextBlock tb = new TextBlock();
                tb.Padding = new System.Windows.Thickness(10);
                Canvas.SetLeft(tb, values[i]);
                Canvas.SetTop(tb, 150);
                tb.Width = 50;
                tb.Height = 50;
                tb.TextAlignment = System.Windows.TextAlignment.Center;
                tb.FontSize = 20;
                tb.Text = str[i].ToString();
                tb.Foreground = Brushes.White;
                tb.Background = Brushes.Blue;
                tbs.Add(tb);
            } 

            return tbs;
        }
    }
}
