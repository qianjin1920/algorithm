﻿/*
 * 说明：设置动画属性。
 * 作者：陈振
 * 日志：2023/04/22 新建此类。
 * 
 * 
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /*internal class SetAnimationProperty
    {
    }*/

    public class AnimationHelper
    {
        public static PropertyPath Left = new PropertyPath(Canvas.LeftProperty);
        public static PropertyPath Top = new PropertyPath(Canvas.TopProperty);

        public static void AddAnimation(Storyboard sb,Shape shape,AnimationTimeline ani,PropertyPath property)
        {
            //ani.BeginTime = sb.Children.Count == 0 ? TimeSpan.Zero : sb.Children[^1].BeginTime + sb.Children[^1].Duration.TimeSpan;
            sb.Children.Add(ani);
            Storyboard.SetTarget(ani, shape);
            Storyboard.SetTargetProperty(ani, property);
        }

    }
}

