﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Shapes;
using System.Drawing.Drawing2D;
using System.IO;
using System.Diagnostics;
//using System.Drawing;
namespace WpfApp1
{
	/// <summary>
	/// MyWindow.xaml 的交互逻辑
	/// </summary>
	public partial class MyWindow : Window
	{
        static Random random = new Random();

        public MyWindow()
		{
			InitializeComponent();
		}

		/*Shape 是 WPF (Windows Presentation Foundation) 中的一个抽象类，
         * 用于表示所有可绘制的 2D 图形对象的基类。Shape 对象有很多派生类，
         * 比如矩形(Rectangle)、椭圆(Ellipse)、多边形(Polygon)等等。
         * 通过在 WPF 应用程序中使用 Shape 对象，可以轻松地创建和操作图形对象。
         */

		public void SetAnimation(Shape shape)
		{
			//定义动画
			var ani = new DoubleAnimation();
			ani.From = 1.0;
			ani.To = 0.0;
			//Duration 是一个结构体，表示持续时间，包含一个 TimeSpan 属性，表示持续时间
			ani.Duration = new Duration(TimeSpan.FromSeconds(3));

			/*当只有一个动画时可以直接使用shape.BeginAnimation(Shape.OpacityProperty,ani);
            就不需要使用故事版*/

			//定义动画作用的故事版
			var storyBoard = new Storyboard();
			storyBoard.Children.Add(ani);
			Storyboard.SetTarget(ani, shape);
			Storyboard.SetTargetProperty(ani, new PropertyPath(Shape.OpacityProperty));

			//开始动画
			storyBoard.Begin(this);
		}

		public void SetAnimation2(Shape shape)
		{
			//定义动画
			var ani1 = new DoubleAnimation();
			ani1.From = 10;
			ani1.To = 200;
			//Duration 是一个结构体，表示持续时间，包含一个 TimeSpan 属性，表示持续时间
			ani1.Duration = new Duration(TimeSpan.FromSeconds(3));
			//自动返回
			ani1.AutoReverse = true;
			//动画一直重复
			ani1.RepeatBehavior = RepeatBehavior.Forever;


			var ani2 = new DoubleAnimation();
			ani2.From = 10;
			ani2.To = 200;
			//Duration 是一个结构体，表示持续时间，包含一个 TimeSpan 属性，表示持续时间
			ani2.Duration = new Duration(TimeSpan.FromSeconds(3));
			//自动返回
			ani2.AutoReverse = true;
			//动画一直重复
			ani2.RepeatBehavior = RepeatBehavior.Forever;

			//定义动画作用的故事版
			var storyBoard = new Storyboard();
			storyBoard.Children.Add(ani1);
			storyBoard.Children.Add(ani2);
			Storyboard.SetTarget(ani1, shape);
			Storyboard.SetTarget(ani2, shape);
			Storyboard.SetTargetProperty(ani1, new PropertyPath(Shape.WidthProperty));
			Storyboard.SetTargetProperty(ani2, new PropertyPath(Shape.HeightProperty));

			//开始动画
			storyBoard.Begin(shape);

		}

		public void addlog(string log)
		{
			using (StreamWriter tw = new StreamWriter("log.txt", true))
			{
				var line = $"{DateTime.Now:yyyy-MM-dd HH:mm:ss} {log}";
				tw.WriteLine(line);
				Debug.WriteLine(line);
			}
		}


		//水平+竖直平移
		public void SetAnimation3(Storyboard sb, Shape shape)
		{
			// X轴动画
			DoubleAnimation animationX = new DoubleAnimation()
			{
				From = Canvas.GetLeft(shape),
				To = Canvas.GetLeft(shape) + 200,
				//BeginTime = sb.Children.Count == 0 ? TimeSpan.Zero : sb.Children[^1].BeginTime + sb.Children[^1].Duration.TimeSpan,
				Duration = TimeSpan.FromSeconds(0.5),
				EasingFunction = new QuadraticEase()// 添加缓动效果
			};
			AnimationHelper.AddAnimation(sb, shape, animationX, AnimationHelper.Left);


            // Y轴动画
            DoubleAnimation animationY = new DoubleAnimation()
			{
				From = Canvas.GetTop(shape),
				To = Canvas.GetTop(shape) + 50,
				BeginTime = animationX.BeginTime + animationX.Duration.TimeSpan,
				Duration = TimeSpan.FromSeconds(0.5),
				EasingFunction = new QuadraticEase()// 添加缓动效果
			};
            AnimationHelper.AddAnimation(sb, shape,animationY,AnimationHelper.Top);

		}


        public void SetAnimation3(Storyboard sb, Shape shape, double x, double y)
        {
            // X轴动画
            DoubleAnimation animationX = new DoubleAnimation()
            {
                From = Canvas.GetLeft(shape),
                To = x,
                //BeginTime = sb.Children.Count == 0 ? TimeSpan.Zero : sb.Children[^1].BeginTime + sb.Children[^1].Duration.TimeSpan,
                Duration = TimeSpan.FromSeconds(0.5),
                EasingFunction = new QuadraticEase()// 添加缓动效果
            };
            AnimationHelper.AddAnimation(sb, shape, animationX, AnimationHelper.Left);


            // Y轴动画
            DoubleAnimation animationY = new DoubleAnimation()
            {
                From = Canvas.GetTop(shape),
                To = y,
                //BeginTime = animationX.BeginTime + animationX.Duration.TimeSpan,
                Duration = TimeSpan.FromSeconds(0.5),
                EasingFunction = new QuadraticEase()// 添加缓动效果
            };
            AnimationHelper.AddAnimation(sb, shape, animationY, AnimationHelper.Top);
        }





        //水平+竖直平移
        public void SetAnimation4(Storyboard sb, Shape shape1, Shape shape2)
        {
			double x1 = Canvas.GetLeft(shape1);
			double y1 = Canvas.GetTop(shape1);
			double x2 = Canvas.GetLeft(shape2);
			double y2 = Canvas.GetTop(shape2); 
            SetAnimation3(sb, shape1, x2, y2);
            SetAnimation3(sb, shape2, x1, y1);
        }

		public void GenerateRectangle()
		{
            Rectangle rec = new Rectangle();
            rec.Height = random.NextInt64(20, 100);
            rec.Width = random.NextInt64(20, 100);
            //填充颜色
            rec.Fill = Brushes.Pink;
            //设置边框颜色
            rec.Stroke = Brushes.Black;
            //设置边框宽度
            rec.StrokeThickness = 3;
            canvas.Children.Add(rec);
            Canvas.SetTop(rec, random.Next(200));
            Canvas.SetLeft(rec, random.Next(500));
        }


        private void btn1_Click(object sender, RoutedEventArgs e)
		{
			GenerateRectangle();
        }


		private void btn2_Click(object sender, RoutedEventArgs e)
		{
			for (int i = 0; i < 10; i++)
				GenerateRectangle();
		}

		private void btn3_Click(object sender, RoutedEventArgs e)
		{
			foreach (Shape shape in canvas.Children)
			{
				SetAnimation(shape);
			}
		}

		private void btn4_Click(object sender, RoutedEventArgs e)
		{
			foreach (Shape shape in canvas.Children)
			{
				SetAnimation2(shape);
			}
		}

		public void InsertSort(int[] arr)
		{
			int n = arr.Length;
			for (int i = 1; i < n; i++)
			{
				int key = arr[i];
				int j = i - 1;

				while (j >= 0 && arr[j] > key)
				{
					arr[j + 1] = arr[j];
					j = j - 1;
				}
				arr[j + 1] = key;
			}
		}

		//生成规则排列的矩形
		private void btn5_Click(object sender, RoutedEventArgs e)
		{
			int[] arr = { 8, 8, 15, 8, 8, 15, 24, 10 };

			//根据数组创建矩形
			for (int i = 0; i < arr.Length; i++)
			{
				Rectangle rec = new Rectangle();
				rec.Height = arr[i] * 10;
				rec.Width = 30;
				//填充颜色
				rec.Fill = Brushes.White;
				//设置边框颜色
				rec.Stroke = Brushes.Black;
				//设置边框宽度
				rec.StrokeThickness = 1;
				canvas.Children.Add(rec);
				//300-rec.Height设置同一水平高度

				Canvas.SetTop(rec, 300 - rec.Height);
				//i * 5 是每个矩形之间的额外间距，这样每个矩形之间的间距就是固定的 30
				Canvas.SetLeft(rec, i * 30 + i * 5 + 30);

			}

		}


		private void btn6_Click(object sender, RoutedEventArgs e)
		{
			// 如果canvas没有图形，则自动添加。
			if (canvas.Children.Count == 0)
				btn1_Click(btn1, new RoutedEventArgs());

			// 定义故事板
			Storyboard sb = new Storyboard();
			sb.Completed += (sender, e) =>   // 故事板结束时，可以在下面添加相关代码。
			{
				MessageBox.Show("Done");
			};


			foreach (Shape shape in canvas.Children)
			{
				SetAnimation3(sb, shape);
			}

			// 开始故事板
			sb.Begin();
		}

		private void Sb_Completed(object? sender, EventArgs e)
		{
			throw new NotImplementedException();
		}

		private void btn7_Click(object sender, RoutedEventArgs e)
		{

		}

		private void btn8_Click(object sender, RoutedEventArgs e)
		{
			System.Diagnostics.Debug.WriteLine("hello");

			int[] arr = { 64, 34, 25, 12, 22, 11, 90 };
			InsertSort(arr);
			Console.WriteLine("Sorted array:");
			for (int i = 0; i < arr.Length; i++)
			{
				//Console.Write(arr[i] + " ");
				System.Diagnostics.Debug.Write(arr[i] + " ");
			}
			//MessageBox.Show("执行完毕");
			double t1 = canvas.Children.Count;
			/*foreach (Shape shape in canvas.Children)
            {
                System.Diagnostics.Debug.WriteLine(Canvas.GetLeft(shape));
            }*/
			System.Diagnostics.Debug.WriteLine(Canvas.GetLeft(canvas.Children[1]));
			System.Diagnostics.Debug.WriteLine(canvas.Children[1]);
			System.Diagnostics.Debug.WriteLine(t1);
		}

		private void btn9_Click(object sender, RoutedEventArgs e)
		{
            // 如果canvas没有图形，则自动添加。
            while (canvas.Children.Count < 2)
                GenerateRectangle();

            // 定义故事板
            Storyboard sb = new Storyboard();

			// sb.Completed += (sender, e) => MessageBox.Show("Swaped."); 
			if(canvas.Children[0] is Shape shape1 && canvas.Children[1] is Shape shape2)
                SetAnimation4(sb, shape1, shape2);
			

            // 开始故事板
            sb.Begin();
        }
	}
}
