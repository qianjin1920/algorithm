﻿using ConsoleAlgorithms.Tests;

namespace ConsoleAlgorithms
{
    internal class Program
    {
        static void Main(string[] args)
        {
            new C01_SinProblem().Run();
        }
    }
}