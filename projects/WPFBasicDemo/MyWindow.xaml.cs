﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WPFDemo2
{
	/// <summary>
	/// MyWindow.xaml 的交互逻辑
	/// </summary>
	public partial class MyWindow : Window
	{
		public MyWindow()
		{
			InitializeComponent();
		}


		public void SetAnimation(Shape shape)
		{
			// 定义动画
			var ani = new DoubleAnimation();
			ani.From = 0.3;
			ani.To = 0.1;
			ani.Duration = new Duration(TimeSpan.FromSeconds(1));



			shape.BeginAnimation(Shape.OpacityProperty, ani);

		}


		public void SetAnimation1(Shape shape)
		{
			// 定义动画
			var ani1 = new DoubleAnimation();
			ani1.From = 100;
			ani1.To = 10;
			ani1.Duration = new Duration(TimeSpan.FromSeconds(10));
			//ani1.AutoReverse = true;
			//ani1.RepeatBehavior = RepeatBehavior.Forever;


			var ani2 = new DoubleAnimation();
			ani2.From = 10;
			ani2.To = 100;
			ani2.BeginTime = TimeSpan.FromSeconds(5); // 决定动画的开始执行时间
			ani2.Duration = new Duration(TimeSpan.FromSeconds(10));
			//ani2.AutoReverse = true;
			//ani2.RepeatBehavior = RepeatBehavior.Forever;

			// 定义动画作用的故事版
			var storyBoard = new Storyboard();
			storyBoard.Children.Add(ani1);
			Storyboard.SetTarget(ani1, shape);
			Storyboard.SetTargetProperty(ani1, new PropertyPath(Shape.WidthProperty));
			storyBoard.Children.Add(ani2);
			Storyboard.SetTarget(ani2, shape);
			Storyboard.SetTargetProperty(ani2, new PropertyPath(Shape.HeightProperty));

			// 开始动画
			storyBoard.Begin(this);
		}

		private void btnTest11_Click(object sender, RoutedEventArgs e)
		{
			Random random = new Random();
			for (int i = 0; i < 10; i++)
			{
				Rectangle rect = new Rectangle();
				rect.Width = random.Next(10, 200);
				rect.Height = random.Next(20, 80);
				rect.Fill = Brushes.Blue;
				rect.Stroke = Brushes.Black;
				rect.StrokeThickness = 0.5;
				canvas.Children.Add(rect);
				Canvas.SetLeft(rect, random.Next(440));
				Canvas.SetTop(rect, random.Next(280));
			}
		}

		private void btnTest12_Click(object sender, RoutedEventArgs e)
		{
			foreach (Shape shape in canvas.Children)
			{
				SetAnimation1(shape);
			}
		}

		List<Rectangle> rects = new List<Rectangle>();

		private void btnTest13_Click(object sender, RoutedEventArgs e)
		{
			int[] values = { 11, 15, 8, 14, 6, 8 };

			for (int i = 0; i < values.Length; i++)
			{
				Rectangle rect = new Rectangle();
				rect.Width = 20;
				rect.Height = values[i] * 20;
				rect.Fill = Brushes.Blue;
				rect.Stroke = Brushes.Black;
				rect.StrokeThickness = 3;
				canvas.Children.Add(rect);
				rects.Add(rect);
				Canvas.SetLeft(rect, i * 30 + 30);
				Canvas.SetTop(rect, 300 - rect.Height);
			}
		}

		private void btnTest14_Click(object sender, RoutedEventArgs e)
		{
			SwitchAnimation(0, 3);
		}

		private void SwitchAnimation1(int id1, int id2)
		{
			var r1 = rects[id1];
			var r2 = rects[id2];

			r1.RenderTransform = new TranslateTransform();
			r2.RenderTransform = new TranslateTransform();

			// 定义动画
			var ani1 = new DoubleAnimation();
			ani1.From = Canvas.GetLeft(r1);
			ani1.To = Canvas.GetLeft(r2);
			ani1.Duration = new Duration(TimeSpan.FromSeconds(3));

			var ani2 = new DoubleAnimation();
			ani2.From = Canvas.GetLeft(r2);
			ani2.To = Canvas.GetLeft(r1); ;
			ani2.Duration = new Duration(TimeSpan.FromSeconds(3));

			// 定义动画作用的故事版
			var storyBoard = new Storyboard();
			storyBoard.Children.Add(ani1);
			Storyboard.SetTarget(ani1, r1);
			Storyboard.SetTargetProperty(ani1, new PropertyPath("RenderTransform.X"));

			storyBoard.Children.Add(ani2);
			Storyboard.SetTarget(ani2, r2);
			Storyboard.SetTargetProperty(ani2, new PropertyPath("RenderTransform.X"));

			// 开始动画
			storyBoard.Begin(this);
		}

		// 将指定的 shape 高亮显示
		public void HighLightAnimation(Shape shape)
		{

		}

		private void SwitchAnimation(int id1, int id2)
		{
			// 定义动画作用的故事版
			var storyBoard = new Storyboard();

			var r1 = rects[id1];
			var r2 = rects[id2];

			r1.RenderTransform = new TranslateTransform();
			r2.RenderTransform = new TranslateTransform();

			// 定义动画
			MoveShapeLeft(storyBoard, r1, 0, Canvas.GetLeft(r1), Canvas.GetLeft(r2));
			MoveShapeLeft(storyBoard, r2, 0, Canvas.GetLeft(r2), Canvas.GetLeft(r1));

			// 开始动画
			storyBoard.Begin(this);
		}

		public void MoveShapeLeft(Storyboard storyBoard, Shape shape, double beginTime, double x1, double x2)
		{
			// X 方向移动
			var da = new DoubleAnimation()
			{
				From = x1,
				To = x2,
				BeginTime = TimeSpan.FromSeconds(beginTime),
				Duration = new Duration(TimeSpan.FromSeconds(3)),
			};

			storyBoard.Children.Add(da);
			Storyboard.SetTarget(da, shape);
			Storyboard.SetTargetProperty(da, new PropertyPath("RenderTransform.X"));
		}

		private void btnTest15_Click(object sender, RoutedEventArgs e)
		{
			btnTest15.Content = "你好";
		}
	}
}
