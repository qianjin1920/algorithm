# 基于WPF的算法可视化
本仓库主要记录了使用WPF制作的排序等算法的可视化研究。


# 会议记录
## 2023/04/17
1. 仔细阅读本项目中的几个示例项目的代码；
2. 多做测试：不确定的知识点需要通过测试来验证，不要想当然（即我“觉得”）；
3. 使用StoryBoard做动画，而不要使用Thread.Sleep()这样的线程操作函数。

# 参考资料
## Git学习网站
* 菜鸟教程 Git 五分钟教程 https://www.runoob.com/w3cnote/git-five-minutes-tutorial.html
* 知乎：Git 最全教程 https://zhuanlan.zhihu.com/p/539611066
* 廖雪峰的官方网站：Git教程 https://www.liaoxuefeng.com/wiki/896043488029600
* 使用SourceTree管理Git https://www.liaoxuefeng.com/wiki/896043488029600/1317161920364578